--Ex1)
--isEven :: Int -> Bool
--isEven n = mod n 2 == 0
--A função mod realiza a divisão de um número por 2 e retorna o resto da divisão

--Ex2)
somaquad :: Int -> Int -> Int
somaquad x y = x^2 + y^2

--Ex3)
doubleFirst :: [Int] -> Int
doubleFirst a = (head a)^2

--Ex4)
hasEqHeads :: [Int] -> [Int] -> Bool 
hasEqHeads l1 l2 = head l1 == head l2

--Ex5)
addMr :: [String] -> [String]
addMr nome = map ("Mr."++) nome

--Ex6)
contaEspaco :: [Char] -> Int
contaEspaco frase = length (filter (==' ') frase)

--Ex7)
func1 :: [Float] -> [Float]
func1 lista = map (\n -> 3*n^2 + 2/n + 1) lista
--func1 lista = map (+1) (zipWith (+) (map (3*) (map(^2) lista)) (map (2/) lista))

--Ex8)
filterIdade :: [Int] -> [Int]
filterIdade lista = filter(< 2015 - 1970) lista

--Ex9)
serie :: Double -> [Double] -> Double
serie m lista = sum (map (/m) lista)

--Ex10)
charFound :: Char -> String -> Bool
charFound c str = null(filter(==c) str) == False

--Ex11)
htmlListItems :: [String]->[String]
htmlListItems str = map(++"</LI>")(map ("<LI>"++) str)

--EX12)
--Entrada: takeWhile (< 5) [1,2,3,4,5]
--Saída: [1,2,3,4]
--Entrada: takeWhile (/=' ') "Fulana de Tal"
--Saída: "Fulana"
--Outro exemplo: 
--Entrada: takeWhile (\x -> 2^x <= 1024) [1..20]
--Saída: [1,2,3,4,5,6,7,8,9,10]

--Ex13)

--1)filtrar o primeiro nome (takeWhile)
--2)retirar o último caractere da string filtrada (last)
--3)comparar com o caractere 'a' (==)
--4)Se o retorno for "true" filtrar o nome da lista original (filter)

--listaFeminino :: [String] -> [String]
--listaFeminino str = map (filter (=='a')(last (map (takeWhile (/=' ')) str))) str

listaFeminino :: [String] -> [String]
listaFeminino str = filter (\x -> last x == 'a')(map (takeWhile (/=' ')) str)


