--Trabalho 5 - Lista de exercícios
--Ex1)
addSuffix :: String -> [String] ->[String]
addSuffix suff str= [x++suff | x<-str]

--Ex2)
addSuffix2 :: String -> [String] -> [String]
addSuffix2 suff [] = []
addSuffix2 suff (x:xs) = (x++suff) : addSuffix2 suff (xs)

--Ex3)
countShorts :: [String] -> Int
countShorts [] = 0
countShorts (x:xs)
 |length x < 5 = 1 + countShorts(xs)
 |otherwise = countShorts(xs)

--Ex4)
countShorts2 :: [String] -> Int
countShorts2 str =  length ([x | x<-str, (length x) < 5])

--Ex5)
ciclo :: Int -> [Int] -> [Int]
ciclo 0 lista = []
ciclo n lista = lista++(ciclo (n-1) lista)

--Ex6)
combine :: [Int] -> [String] -> [(Int,String)]
combine [] [] = []
combine (x:xs) (y:ys) = (x,y): (combine (xs) (ys))

--Ex7)
numera :: [String] -> [(Int, String)]
numera [] = []
numera lista = numera(init lista)++[(length lista, last lista)]

--Ex9)
crossProduct :: [a] -> [b] -> [(a,b)] 
crossProduct [] ys   = []
crossProduct (a:b) ys= (pairWithAll a ys)++(crossProduct b ys)

pairWithAll :: a -> [b] -> [(a,b)]
pairWithAll elemento []    = []
pairWithAll elemento (x:xs) = (elemento, x):(pairWithAll elemento xs)

--Ex13)
func :: [(Int,Int)] -> ([Int],[Int])
func lista = (map fst lista,map snd lista)