import Text.Printf

type Point = (Float,Float) 
type Rect = (Point,Float,Float)
type RGB = (Int,Int,Int)--Define formato para cores no padrão RGB
type HSL = (Float,Float,Float)--Define formato para cores no padrão HSL

--Função que faz conversão do código HSL para RGB (De acordo com algoritmo apresentado em [1])
hsl2rgb :: HSL -> RGB 
hsl2rgb (h',s',l') = (\(x,y,z) -> (round(r*255),round(g*255),round(b*255))) (h',s',l')
 where
  h = h'/360 
  s = s'/100
  l = l'/100
  m2 = if l<=0.5 then l*(s+1) else l+s-l*s
  m1 = 2*l-m2
  r = aux_hsl2rgb (m1,m2,h+1/3)
  g = aux_hsl2rgb (m1,m2,h)
  b = aux_hsl2rgb (m1,m2,h-1/3)

--Função que auxilia a execução de hsl2rgb 
aux_hsl2rgb :: (Float,Float,Float)->Float
aux_hsl2rgb (m1,m2,t)
 |h*6<1 = m1+(m2-m1)*h*6
 |h*2<1 = m2
 |h*3<2 = m1+(m2-m1)*(2/3-h)*6
 |otherwise = m1
 where
  h = if t<0 then t+1 else if t>1 then t-1 else t

--Função que gera lista de cores RGB
geraLstRGB :: Float->(Float,Float)->[RGB]
geraLstRGB hue (m,n) = [if (n,m) == (1,1) then hsl2rgb (hue,100,50) else hsl2rgb (hue,s,l)| s<-[100,s_passo..0], l<-[100,l_passo..0]]
 where
  s_passo = if n==1 then 0 else 100*(n-2)/(n-1)
  l_passo = if m==1 then 0 else 100*(m-2)/(m-1)   

--Função que gera os retângulos da paleta
geraRects :: (Float, Float)->(Float,Float)->(Float,Float)->[Rect]
geraRects (m,n) (w,h) (be,bi) =  [((x,y), w, h) | x <- [be,be+(w+bi)..be+(n-1)*(w+bi)], y <- [be,be+(h+bi)..be+(m-1)*(h+bi)]]

--Função que auxilia a função geraPaleta
aux_geraPaleta :: [RGB] -> [Rect] -> [(RGB,Rect)]
aux_geraPaleta cores rtgs = zipWith (\x y -> (x,y)) cores rtgs

--Função que gera uma paleta com cor básica definida pelo argumento hue, combinando cada retângulo da matriz com seu respectivo código RGB 
geraPaleta :: (Float,(Float,Float))->(Float,Float,Float,Float)->[(RGB,Rect)]
geraPaleta (hue,(m,n)) (w,h,be,bi) = aux_geraPaleta (geraLstRGB hue (m,n)) (geraRects (m,n) (w,h) (be,bi))

--Escreve cada linha do código SVG com a formatação de cada retângulo que forma a paleta
writeLinha :: (RGB,Rect) -> String 
writeLinha ((r,g,b),((x,y),w,h)) = 
  printf "<rect x='%.3f' y='%.3f' width='%.2f' height='%.2f' style='fill:rgb(%d,%d,%d);stroke:black;stroke-width:1;'/>\n" x y w h r g b

--Escreve o código para a criação do arquivo SVG
writeSVGcode :: (Float,Float) -> [(RGB,Rect)] -> String 
writeSVGcode (w,h) paleta = 
 printf "<svg width='%.2f' height='%.2f' xmlns='http://www.w3.org/2000/svg'>\n" w h 
      ++ (concatMap writeLinha paleta)  ++ "</svg>"

main :: IO ()
main = do
  let
    hue = 210 --matiz da paleta
    m = 9 --número de linhas
    n = 5 --número de colunas
    w = 90 --largura de cada retângulo da paleta
    h = 40 --altura de cada retângulo da paleta
    be = 10 --vão entre os retângulos externos e o fundo
    bi = 2 --vão entre dois retângulos adjascentes
    args = (hue,(m,n))
    form = (w,h,be,bi)
    paleta = geraPaleta args form
    svg_dim = (2*be+n*w+(n-1)*bi, 2*be+ m*h+ (m-1)*bi) --lei de dimensionamento do fundo SVG (bordas brancas)
  writeFile "t6-p1.svg" $ writeSVGcode svg_dim paleta

--REFERÊNCIAS
--[1]http://www.w3.org/TR/css3-color/#hsl-color

