import Text.Printf

type Point = (Float,Float) 
type Rect = (Point,Float,Float)
type RGB = (Int,Int,Int)--Define formato para cores no padrão RGB
type HSL = (Float,Float,Float)--Define formato para cores no padrão HSL

--Função que faz conversão do código HSL para RGB (De acordo com algoritmo apresentado em [1])
hsl2rgb :: HSL -> RGB 
hsl2rgb (h',s',l') = (\(x,y,z) -> (round(r*255),round(g*255),round(b*255))) (h',s',l')
 where
  h = h'/360 
  s = s'/100
  l = l'/100
  m2 = if l<=0.5 then l*(s+1) else l+s-l*s
  m1 = 2*l-m2
  r = aux_hsl2rgb (m1,m2,h+1/3)
  g = aux_hsl2rgb (m1,m2,h)
  b = aux_hsl2rgb (m1,m2,h-1/3)

--Função que auxilia a execução de hsl2rgb 
aux_hsl2rgb :: (Float,Float,Float)->Float
aux_hsl2rgb (m1,m2,t)
 |h*6<1 = m1+(m2-m1)*h*6
 |h*2<1 = m2
 |h*3<2 = m1+(m2-m1)*(2/3-h)*6
 |otherwise = m1
 where
  h = if t<0 then t+1 else if t>1 then t-1 else t

--Função que gera lista de cores RGB
geraLstRGB :: Float->(Float,Float)->[RGB]
geraLstRGB hue (m,n) = [if (n,m) == (1,1) then hsl2rgb (hue,100,50) else hsl2rgb (hue,s,l)| s<-[100,s_passo..0], l<-[100,l_passo..0]]
 where
  s_passo = if n==1 then 0 else 100*(n-2)/(n-1)
  l_passo = if m==1 then 0 else 100*(m-2)/(m-1)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Definições:
-- 1) As matizes serão divididas simetricamente: def = 360/(lin*col) (def = defasagem angular das matizes entre as paletas da matriz)
-- 2) Haverá uma constante (hue) para definir o início da lista de "matizes", que corresponderá a matiz da paleta na posição (1,1) da matriz.
-- p.ex - Se a matriz de paletas tem dimensão (2x2) e o usuário definiu a primeira matiz como hue_0 = 232°:
--	Def = 360/(2x2) = 90° 
--  matizes = [232, 322, 52, 142]      

--Função que gera lista de matizes simétricas a partir do número de linhas e colunas da matriz da paletas e do valor de uma matiz inicial
geraHue :: (Float,Float)->Float->[Float]
geraHue (lin,col) hue = geraHueLst (lin*col) hue (lin,col)

--Função recursiva auxiliar à geraHue que faz toda a lógica de gração para função principal.
geraHueLst :: Float->Float->(Float,Float)->[Float]
geraHueLst i hue (lin,col) = if i==0 then [] else [myMod hue 360]++geraHueLst (i-1) (myMod (hue+360/(lin*col)) 360) (lin,col)

--Função auxiliar que calcula o resto (mod) de uma divisão: num/den. Foi necessário criar esta função pois a função "mod" de prelude não 
--funciona para tipos Float
myMod :: Float->Float->Float
myMod num den
 |num<den = num
 |num==den = 0
 |otherwise =myMod (num-den) den

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Função que gera uma lista  de códigos RGB associado a cada elemento da matriz da paleta.
--O argumento "hue" define a matiz do paleta na posição (1,1) da matriz de paletas
--O argumento (m,n) é o número de linhas e colunas de cada paleta
--O argumento (lin,col) é o número de linhas e colunas da matriz de paletas
geraMatrizRGB :: Float->(Float,Float)->(Float,Float)->[RGB]
geraMatrizRGB hue (m,n) (lin,col) = [y | x <-(geraHue (lin,col) hue), y<-geraLstRGB x (m,n)]

--Função que gera a lista que define os retângulos da matriz de paletas
geraRetMatriz :: (Float,Float,Float,Float)->(Float,Float,Float,Float)->(Float,Float,Float)->[(Rect)]
geraRetMatriz (m,n,w,h) (lin,col,alt,lrg) (be,bi,bp) = [(((x,y), w, h)) | xm <- lst_xm, ym <- lst_ym, x <- [xm, xm+(w+bi)..xm+(n-1)*(w+bi)], y <- [ym, ym+(h+bi)..ym+(m-1)*(h+bi)]]
 where
  lst_xm = [be, be+(lrg+bp)..be+(col-1)*(lrg+bp)]
  lst_ym = [be, be+(alt+bp)..be+(lin-1)*(alt+bp)]

--Função que gera matriz de paletas com código RGB associado a cada retângulo
geraMatrizPaletas :: (Float,(Float,Float))-> ((Float,Float),(Float,Float))->(Float,Float,Float,Float,Float)->[(RGB,Rect)]
geraMatrizPaletas (hue,(lin,col)) ((m,n),(w,h)) (alt,lrg,bp,be,bi) = zipWith (\x y -> (x,y)) matrizRGB matrizRect
 where
  matrizRGB = (geraMatrizRGB hue (m,n) (lin,col))
  matrizRect = (geraRetMatriz (m,n,w,h) (lin,col,alt,lrg) (be,bi,bp))
-----------------------------------------------------------------------------------------------
--Escreve cada linha do código SVG com a formatação de cada retângulo que forma a paleta
writeLinha :: (RGB,Rect) -> String 
writeLinha ((r,g,b),((x,y),w,h)) = 
  printf "<rect x='%.3f' y='%.3f' width='%.2f' height='%.2f' style='fill:rgb(%d,%d,%d);stroke:black;stroke-width:1;'/>\n" x y w h r g b

--Escreve o código para a criação do arquivo SVG
writeSVGcode :: (Float,Float) -> [(RGB,Rect)] -> String 
writeSVGcode (w,h) paletas = 
 printf "<svg width='%.2f' height='%.2f' xmlns='http://www.w3.org/2000/svg'>\n" w h 
      ++ (concatMap writeLinha paletas)  ++ "</svg>"

main :: IO ()
main = do
  let
    hue = 0 --matiz inicial (posição (1,1) da matriz de paletas)
    lin = 4 --número de linhas da matriz de paletas
    col = 3 --número de colunas da matriz de paletas
    m = 18 --número de linhas de cada paleta
    n = 10 --número de colunas de cada palea
    w = 90 --largura de cada retângulo de uma paleta
    h = 40 --altura de cada retângulo de uma paleta
    bp = 25 --vão entre paletas adjascentes
    be = 15 --vão entre os retângulos externos e o fundo
    bi = 5 --vão entre dois retângulos adjascentes
    alt = m*h+(m-1)*bi --altura de cada paleta (utilizado para cálculos internos)
    lrg = n*w+(n-1)*bi --largura de cada paleta (utilizado para cálculos internos)
    args_mtx = (hue,(lin,col))
    args_plt = ((m,n),(w,h)) 
    form = (alt,lrg,bp,be,bi)
    paletas = geraMatrizPaletas args_mtx args_plt form  
    svg_dim = (2*be + col*lrg + (col-1)*bp, 2*be + lin*alt + (lin-1)*bp) --lei de dimensionamento do fundo SVG (bordas brancas)
  writeFile "t6-p2.svg" $ writeSVGcode svg_dim paletas

--REFERÊNCIAS
--[1]http://www.w3.org/TR/css3-color/#hsl-color
