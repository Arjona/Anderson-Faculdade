--Ex1) 
eleva2:: [Int] -> [Int]
eleva2 [] = []
eleva2 (x:xs) = x^2 : eleva2 xs 

--Ex2)
contido :: Char -> String -> Bool
contido c "" = False
contido c s
 | c == head s = True
 | otherwise = contido c (tail s)

--Ex3)
semVogais :: String -> String
semVogais "" = ""
semVogais (x:xs)
 | x == 'a'  = drop 1 (x : semVogais xs)
 | x == 'e'  = drop 1 (x : semVogais xs)
 | x == 'i'  = drop 1 (x : semVogais xs)
 | x == 'o'  = drop 1 (x : semVogais xs)
 | x == 'u'  = drop 1 (x : semVogais xs)
 | x == 'A'  = drop 1 (x : semVogais xs)
 | x == 'E'  = drop 1 (x : semVogais xs)
 | x == 'I'  = drop 1 (x : semVogais xs)
 | x == 'O'  = drop 1 (x : semVogais xs)
 | x == 'U'  = drop 1 (x : semVogais xs)
 | otherwise = x : semVogais xs
 
-- Ex4)
translate :: [(Double,Double)] -> [(Double,Double)]
translate [] = []
translate t = map (\(x,y) -> (x+2,y+2)) t

-- Ex5)
geraTabela :: Int -> [(Int,Int)] 
geraTabela n = aux 0 n

aux :: Int -> Int -> [(Int,Int)]
aux x n = if (x <= n)
 then (x, x^2) : aux (x+1) n
 else []
